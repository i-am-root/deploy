#!/bin/bash -eu

tar --exclude="./deployment" --exclude="./environment" -czf deployment/docker/app/package.tar.gz .
